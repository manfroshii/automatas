
import java.io.*;

public class Main {
   public static void main(String [] args) {
      File archivoFail = null;
      File archivoTest = null;
      FileReader fr = null;
      BufferedReader br = null;

      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivoFail = new File ("C:\\Users\\manfred\\Documents\\repositorios\\automatas\\examples\\fail.mile");
         archivoTest = new File ("C:\\Users\\manfred\\Documents\\repositorios\\automatas\\examples\\test.mile");
         fr = new FileReader (archivoFail);
         br = new BufferedReader(fr);

         // Lectura del fichero
         String linea;
         while((linea=br.readLine())!=null)
            System.out.println(linea);
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
   }
}
