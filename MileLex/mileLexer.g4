/**
 * Define a grammar called Hello
 */
lexer grammar mileLexer;

/*
 * op
 */

PLUS : '+';
//
EXP : '^';


/*
 * structures
 */

fragment TRUE : 'true';
fragment FALSE: 'false';
FOR : 'for';
START_T : 'start';
INT_TYPE : 'int';
REAL_TYPE : 'real';
BOOL_TYPE : 'bool'; 
STRING_TYPE : 'string';

BEGIN_RW : 'begin'; 
END_RW : 'end';
IF_RW : 'if';
ELSE_RW : 'else';
WHILE_RW : 'while';
FOR_RW : 'for';

WRITE_RW : 'write';
READ_RW : 'read';

TRUE_LITERAL : 'true';
FALSE_LITERAL : 'false';


//

UNARY : '-'|'+';
fragment NAT : [0-9];
NUMBER :  NAT+;


//
ID : [a-z]+ ;             // match lower-case identifiers

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

